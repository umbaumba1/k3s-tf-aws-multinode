# Public Subnet 1.
resource "aws_subnet" "sbnt_public1" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.aws_sbnt_public1
  availability_zone       = "us-east-1a"
  map_public_ip_on_launch = true

  tags = {
    Name = "${var.project_name}_1a-public"
  }
}

# Private Subnet 1.
resource "aws_subnet" "sbnt_private1" {
  vpc_id            = aws_vpc.vpc.id
  cidr_block        = var.aws_sbnt_private1
  availability_zone = "us-east-1a"

  tags = {
    Name = "${var.project_name}_1a-private"
  }
}
