# OUTPUTS
# `terraform output -json | jq .[].value | jq .[]`

output "instance_public_ip-master" {
  value = aws_instance.master.*.public_ip
}

output "instance_public_ip-worker1" {
  value = aws_instance.worker1.public_ip
}

output "instance_public_ip-worker2" {
  value = aws_instance.worker2.public_ip
}

output "instance_public_ip-worker3" {
  value = aws_instance.worker3.public_ip
}
